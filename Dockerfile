FROM java:8
EXPOSE 8080
ADD /target/ZuulService-0.0.1-SNAPSHOT.jar ZuulService-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "ZuulService-0.0.1-SNAPSHOT.jar"]